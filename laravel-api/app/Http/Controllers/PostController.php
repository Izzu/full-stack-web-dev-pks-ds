<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar post berhasil ditampilkan'
            'data'    => $posts  
        ]);   
    }


}
